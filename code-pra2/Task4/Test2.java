package Task4;
/*
 * ketika program ini dijalankan, maka obj1 dan obj2 akan menghasilkan output yang sama
 * kesimpulannya adalah jika kita membuat suatu class yang  didalamnya terdapat method untuk membuat object dari class yang sama
 * maka untuk memanggil method yang ada dalam kelas tersebut bisa dengan 2 cara, yaitu:
 * 1. memanggil method dengan hanya membuat objek baru
 * 2. memanggil method dengan membuat objek baru kemudian mengakses method class
 */
public class Test2 {
	int a = 1; //deklarasi  variabel a dan menetapkan nilainya menjadi 1
	int b = 2; //deklarasi  variabel b dan menetapkan nilainya menjadi 2

	public Test2 func(Test2 obj)
	{
		Test2 obj3 = new Test2(); //deklarasi obj3 yang merupakan objek dari class Test2
		obj3 = obj; //menetapkan nilai obj3 menjadi obj(dari parameter func)
		obj3.a = obj.a++ + ++obj.b; /* obj3.a = obj.1++ (post-increment) +  ++obj(pre-increment).b
		nilainya 4*/
		obj.b = obj.b;
		return obj3; //mengembalikan nilai obj3
	}

	public static void main(String[] args) //fungsi yang akan dijalankan
	{
		Test2 obj1 = new Test2();  /*deklarasi variabel obj1 yang merupakan objek dari class Test2*/  
		Test2 obj2 = obj1.func(obj1); //deklarasi variabel obj2 dengan nilai objek obj1 dan mengambil method func

		System.out.println("obj1.a = " + obj1.a + " obj1.b = " + obj1.b);
		System.out.println("obj2.a = " + obj2.a + " obj1.b = " + obj2.b);
    System.out.println("obj2 = " + obj2);
    System.out.println("obj1 = " + obj1);

	}
}
