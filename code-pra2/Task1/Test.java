package Task1;
/*
 * saat kode ini dijalankan, maka akan menjalankan method start
 * dimana yang pertama kali dilakukan method start adalah menetapkan nilai variabel stra dengan "do"
 * kemudian menetapkan nilai strb dengan memanggil method, dimana method akan menetapkan nilai stra dengan stra + "good"
 * sekarang nilai stra didalam method menjadi "dogood"
 * method akan menampilakn nilai stra yaitu "dogood" kemudian mengembalikan nilai stra menjadi " good"
 * output sementara adalah dogood
 * kemudian proses method start berlanjut yaitu menampilkan string (": " + stra +strb)
 * yang outputnya adalah ": do good"
 * maka output yang dihasilkan program ini adalah string "dogood: do good"
 */

public class Test {
    public static void main(String[] args) {
        Test obj = new Test(); //deklarasi variabel obj yang merupakan object baru dari blueprint class Test
        obj.start(); //memanggil method start dari obj
    }
    public void start() { //method start
        String stra = "do"; //deklarasi nilai stra
        String strb = method(stra); //deklarasi nilai strb
        System.out.print(": "+stra + strb); //perintah menampilan output
    }
    public String method(String stra) { //method method dengan parameter stra
        stra = stra + "good"; //menetapkan nilai stra
        System.out.print(stra); //menampillakan nilai stra
        return " good"; //mengembalikan nilai stra menjadi " good"
    }
}