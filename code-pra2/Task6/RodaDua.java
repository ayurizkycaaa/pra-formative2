public class RodaDua {
    private String merek, warna;
    public final int roda = 2;

    public void setMerek(String merek){
        this.merek = merek;
    }

    public void setWarna(String warna){
        this.warna = warna;
    }


    public String getMerek(){
        return this.merek;
    }
    public String getWarna(){
        return this.warna;
    }
}
