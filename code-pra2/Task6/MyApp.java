public class MyApp {
    public static void main(String[] args){
        //objek sepeda
        Sepeda sepeda = new Sepeda();

        //set atribut sepeda
        sepeda.setMerek("Polygon");
        sepeda.setWarna("Hijau");
        sepeda.setJumlahSadel(2);

        //get atribut sepeda
        System.out.println("SEPEDA SAYA");
        System.out.println("Merek : " + sepeda.getMerek());
        System.out.println("Warna : " + sepeda.getWarna());
        System.out.println("Jumlah Sadel : " + sepeda.getJumlahSadel());
        System.out.println("Jumlah Roda : " + sepeda.roda);

        //objek motor
        SepedaMotor motor = new SepedaMotor();

        //set atribut motor
        motor.setMerek("Yamaha");
        motor.setWarna("Kuning");
        motor.setMesinPembakaran("Yamaha");

        //get atribut motor
        System.out.println();
        System.out.println("MOTOR SAYA");
        System.out.println("Merek : " + motor.getMerek());
        System.out.println("Warna : " + motor.getWarna());
        System.out.println("Mesin Pembakaran : " + motor.getMesinPembakaran());
        System.out.println("Jumlah Roda : " + motor.roda);
    }
}