package Task5;
/*
 * Kode ini akan menampilkan array yang dipisah dengan karakter spasi
 * outputnya adalah: 1 2 3
 */

class Test5 { 
public static void main(String[] args)
    {
        int arr[] = { 1, 2, 3 };  //menetapkan nilai array arr

        // final with for-each statement
        for (final int i : arr) //melakukan perulangan untuk memanggil nilai yang ada didalam array arr
            System.out.print(i + " "); //mencetak output isi array (i) kemudian spasi
    }
}

/*
 * output akhirnya adalah:
 * 1 2 3
 */